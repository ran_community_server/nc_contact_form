<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2012 Leo Feyer
 * 
 * @package   NC Contact Form
 * @author    Marcel Mathias Nolte
 * @copyright Marcel Mathias Nolte 2013
 * @website   https://www.noltecomputer.com
 * @license   <marcel.nolte@noltecomputer.de> wrote this file. As long as you retain this notice you
 *            can do whatever you want with this stuff. If we meet some day, and you think this stuff 
 *            is worth it, you can buy me a beer in return. Meanwhile you can provide a link to my
 *            homepage, if you want, or send me a postcard. Be creative! Marcel Mathias Nolte
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_nc_contact_form']['name']    = array('Vor- und Familienname', 'Bitte geben Sie den vollst. Namen ein.');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['phone']   = array('Telefonnummer', 'Bitte geben Sie die Telefonnummer ein.');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['email']   = array('E-Mail-Adresse', 'Bitte geben Sie eine gültige E-Mail-Adresse ein.');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['message'] = array('Nachricht', 'Bitte geben Sie eine Nachricht ein.');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['date']    = array('Datum', '');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['ip']      = array('IP-Adresse', '');

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_nc_contact_form']['sender_legend'] = 'Nachricht';
$GLOBALS['TL_LANG']['tl_nc_contact_form']['data_legend']   = 'Metadaten';

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_nc_contact_form']['show']   = array('Nachricht anzeigen', 'Nachricht ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['delete'] = array('Nachricht löschen', 'Nachricht ID %s löschen');

?>