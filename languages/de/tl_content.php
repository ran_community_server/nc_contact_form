<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2012 Leo Feyer
 * 
 * @package   NC Contact Form
 * @author    Marcel Mathias Nolte
 * @copyright Marcel Mathias Nolte 2013
 * @website   https://www.noltecomputer.com
 * @license   <marcel.nolte@noltecomputer.de> wrote this file. As long as you retain this notice you
 *            can do whatever you want with this stuff. If we meet some day, and you think this stuff 
 *            is worth it, you can buy me a beer in return. Meanwhile you can provide a link to my
 *            homepage, if you want, or send me a postcard. Be creative! Marcel Mathias Nolte
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_content']['redirect_legend']                 = 'Weiterleitung';
$GLOBALS['TL_LANG']['tl_content']['config_legend']                   = 'Konfiguration';
$GLOBALS['TL_LANG']['tl_content']['nc_contact_form_disable_captcha'] = array('Keine Sicherheitsfrage', 'Im Formular kein Captcha verwenden.');
$GLOBALS['TL_LANG']['tl_content']['nc_contact_form_site']            = array('Kontaktformular', 'Zu verwendende Formularvorlage ausw&auml;hlen.');
$GLOBALS['TL_LANG']['tl_content']['nc_contact_form_jump_to']         = array('Weiterleitungsziel', 'Bitte wählen Sie das Weiterleitungsziel aus.');
$GLOBALS['TL_LANG']['tl_content']['nc_contact_form_template']        = array('Template', 'Bitte wählen Sie ein Template aus.');

?>