<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2012 Leo Feyer
 * 
 * @package   NC Contact Form
 * @author    Marcel Mathias Nolte
 * @copyright Marcel Mathias Nolte 2013
 * @website   https://www.noltecomputer.com
 * @license   <marcel.nolte@noltecomputer.de> wrote this file. As long as you retain this notice you
 *            can do whatever you want with this stuff. If we meet some day, and you think this stuff 
 *            is worth it, you can buy me a beer in return. Meanwhile you can provide a link to my
 *            homepage, if you want, or send me a postcard. Be creative! Marcel Mathias Nolte
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['title']         = array('Bezeichnung', 'Bitte geben Sie eine Bezeichnung für das Formular ein.');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['subject']       = array('E-Mail-Betreff', 'Bitte geben Sie die E-Mail-Betreffzeile ein.');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['mail_template'] = array('E-Mail-Template', 'Bitte wählen Sie ein E-Mail-Template aus.');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['new']           = array('Neues Kontaktformular', 'Ein neues Kontaktformular erzeugen');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['show']          = array('Kontaktformular anzeigen', 'Kontaktformular ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['delete']        = array('Kontaktformular löschen', 'Kontaktformular ID %s löschen');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['edit']          = array('Kontaktformular bearbeiten', 'Kontaktformular ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['copy']          = array('Kontaktformular kopieren', 'Kontaktformular ID %s kopieren');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['comments']      = array('Nachrichten anzeigen', 'Nachrichten des Kontaktformulares ID %s anzeigen');

?>