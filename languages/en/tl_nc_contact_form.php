<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2012 Leo Feyer
 * 
 * @package   NC Contact Form
 * @author    Marcel Mathias Nolte
 * @copyright Marcel Mathias Nolte 2013
 * @website   https://www.noltecomputer.com
 * @license   <marcel.nolte@noltecomputer.de> wrote this file. As long as you retain this notice you
 *            can do whatever you want with this stuff. If we meet some day, and you think this stuff 
 *            is worth it, you can buy me a beer in return. Meanwhile you can provide a link to my
 *            homepage, if you want, or send me a postcard. Be creative! Marcel Mathias Nolte
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_nc_contact_form']['name']    = array('Full name', 'Please enter the full name.');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['phone']   = array('Phone number', 'Please enter the phone number.');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['email']   = array('E-mail address', 'Please enter a valid e-mail address.');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['message'] = array('Message', 'Please enter a message.');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['date']    = array('Date', '');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['ip']      = array('IP address', '');


/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_nc_contact_form']['sender_legend'] = 'Message';
$GLOBALS['TL_LANG']['tl_nc_contact_form']['data_legend']   = 'Meta data';


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_nc_contact_form']['show']   = array('Message details', 'Show the details of message ID %s');
$GLOBALS['TL_LANG']['tl_nc_contact_form']['delete'] = array('Delete message', 'Delete message ID %s');

?>