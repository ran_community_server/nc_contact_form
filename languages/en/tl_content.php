<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2012 Leo Feyer
 * 
 * @package   NC Contact Form
 * @author    Marcel Mathias Nolte
 * @copyright Marcel Mathias Nolte 2013
 * @website   https://www.noltecomputer.com
 * @license   <marcel.nolte@noltecomputer.de> wrote this file. As long as you retain this notice you
 *            can do whatever you want with this stuff. If we meet some day, and you think this stuff 
 *            is worth it, you can buy me a beer in return. Meanwhile you can provide a link to my
 *            homepage, if you want, or send me a postcard. Be creative! Marcel Mathias Nolte
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_content']['redirect_legend']                 = 'Redirection';
$GLOBALS['TL_LANG']['tl_content']['config_legend']                   = 'Settings';
$GLOBALS['TL_LANG']['tl_content']['nc_contact_form_disable_captcha'] = array('No security question', 'Do not show a captcha in the form.');
$GLOBALS['TL_LANG']['tl_content']['nc_contact_form_site']            = array('Contact form', 'Form group to use.');
$GLOBALS['TL_LANG']['tl_content']['nc_contact_form_jump_to']         = array('Redirection target', 'Please choose a redirection target.');
$GLOBALS['TL_LANG']['tl_content']['nc_contact_form_template']        = array('Template', 'Please choose a template file.');

?>