<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2012 Leo Feyer
 * 
 * @package   NC Contact Form
 * @author    Marcel Mathias Nolte
 * @copyright Marcel Mathias Nolte 2013
 * @website   https://www.noltecomputer.com
 * @license   <marcel.nolte@noltecomputer.de> wrote this file. As long as you retain this notice you
 *            can do whatever you want with this stuff. If we meet some day, and you think this stuff 
 *            is worth it, you can buy me a beer in return. Meanwhile you can provide a link to my
 *            homepage, if you want, or send me a postcard. Be creative! Marcel Mathias Nolte
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['title']         = array('Title', 'Please enter a title for this form.');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['subject']       = array('Email subject', 'Please enter the mail subject.');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['mail_template'] = array('Email template', 'Please choose a mail template.');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['new']           = array('New contact form', 'Create new contact form');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['show']          = array('Show contact form', 'Show contact form ID %s');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['delete']        = array('Delete contact form', 'Delete contact form ID %s');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['edit']          = array('Edit contact form', 'Edit contact form ID %s');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['copy']          = array('Duplicate contact form', 'Duplicate contact form ID %s');
$GLOBALS['TL_LANG']['tl_nc_contact_form_sites']['comments']      = array('Show messages', 'Show messages of contact form ID %s anzeigen');

?>